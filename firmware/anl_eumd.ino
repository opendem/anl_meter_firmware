#include <WiFi.h>
#include <FS.h>
#include "SD_MMC.h"
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include "SPIFFS.h"
#include <HardwareSerial.h>
#include <ModbusMaster.h>
#include "SPIFFS.h"
#include <NTPClient.h>
#include <WiFiUdp.h>

#define NTP_OFFSET  -3  * 60 * 60 // In seconds
#define NTP_INTERVAL 60 * 1000    // In miliseconds

//#define NTP_ADDRESS  "0.pool.ntp.org"
WiFiUDP ntpUDP;
//NTPClient timeClient(ntpUDP, NTP_ADDRESS, NTP_OFFSET, NTP_INTERVAL);
NTPClient timeClient(ntpUDP, "0.pool.ntp.org", 0, 60000);
//NTPClient timeClient(ntpUDP, NTP_OFFSET);
#define FORMAT_SPIFFS_IF_FAILED true
//#define MQTT_MAX_PACKET_SIZE 5000

HardwareSerial MySerial1(1);
ModbusMaster node;
#define SSerialTxControl 32
char ssid[20];
char password[20];
const char *log_enable = "";
const char *device_name = "";
const char *time_range = "";
const char *channel_data = "";
const char *timeInterval = "";
const char *toDate = "";
const char *fromDate = "";
char data5[2000];
/* topics */
#define TEMP_TOPIC    "tes_topic"
String macaddress,channleTopic,timeIntervalTopic,deviceInfoTopic,channleTopicInitiate,timeIntervalTopicInitiate,deviceInfoTopicInitiate,channleTopicUpdate,timeIntervalTopicUpdate,deviceInfoTopicUpdate,dayStamp,dayStampStart,formattedTime,formattedDate,retriveData,retriveErrorLog,formattedDateStart,folderPath,filePathLog,hrs,filePath,meterStatus,strLog,channelData;
int splitT,splitTStart,LINELEN,len_channel;
unsigned long unixtim;
String logFlag = "False";
String retriveFlag = "False";
char mac9[40],macaddress1[40],channleTopic1[40],timeIntervalTopic1[40],deviceInfoTopic1[40],channleTopicInitiate1[40],timeIntervalTopicInitiate1[40],deviceInfoTopicInitiate1[40],channleTopicUpdate1[40],timeIntervalTopicUpdate1[40],deviceInfoTopicUpdate1[40],meterStatus1[40],retriveData1[40],filePath1[40],folderPath1[40],filePathLog1[40],pathLog1[40],pathLog4[40], retriveErrorLog1[40],channelData1[40];
String channels[19];
char char_channel[5];
//String channels[19];
char channelName[19];
/* Mac Address */
byte mac[6];

const char* mqtt_server = "m10.cloudmqtt.com";
//const char* mqtt_server = "52.14.78.255";
void callback(char* topic, byte* payload, unsigned int length);
WiFiClient espClient;
PubSubClient client(espClient);
const char* mqttUser = "hxfhukvi";
const char* mqttPassword = "qS47hLxRei1l";

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.println((char *)topic);
  if (strcmp(topic,channleTopicUpdate1)==0){
    Serial.println("Got payload from channel update");
   if(!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED)){
      Serial.println("SPIFFS Mount Failed");
      return;
    }
   byte* p = (byte*)malloc(length);
   memcpy(p,payload,length);
   channel_data = (char*)p;
   StaticJsonBuffer<2500> JSONBufferChannel1;
   char channel_data1[2500];
   if(SPIFFS.exists("/channels.txt")){
       Serial.println("File already Exists");
       JsonObject& parsedChannel1 = JSONBufferChannel1.parseObject(String(channel_data));
       parsedChannel1.printTo((char*)channel_data1, parsedChannel1.measureLength() + 1);
       deleteFile(SPIFFS, "/channels.txt");
       writeFile(SPIFFS, "/channels.txt",channel_data1);
       free(p);
    }else{
       JsonObject& parsedChannel2 = JSONBufferChannel1.parseObject(String(channel_data));
       parsedChannel2.printTo((char*)channel_data1, parsedChannel2.measureLength() + 1);
       writeFile(SPIFFS, "/channels.txt", channel_data1);
       free(p);
    }
  }
  if (strcmp(topic,timeIntervalTopicUpdate1)==0) {
     if(!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED)){
      Serial.println("SPIFFS Mount Failed");
      return;
    }
    StaticJsonBuffer<300> timeJSONbuffer;
    JsonObject& timeJSONencoder = timeJSONbuffer.createObject();
    byte* p = (byte*)malloc(length);
    memcpy(p,payload,length);
    timeInterval = (char*)p;
    StaticJsonBuffer<300> JSONBuffertime;
    char timeJSONmessageBuffer[100];
    if(SPIFFS.exists("/time.txt")){
       JsonObject& parsedTime = JSONBuffertime.parseObject(String(timeInterval));
       timeJSONencoder["timeInterval"] = parsedTime["timeInterval"];
       timeJSONencoder.printTo(timeJSONmessageBuffer, sizeof(timeJSONmessageBuffer));
       deleteFile(SPIFFS, "/time.txt");
       Serial.println(timeJSONmessageBuffer);
       writeFile(SPIFFS, "/time.txt", timeJSONmessageBuffer);
       char JSONmessageBufferTime[500];
       String strTime = readFile(SPIFFS, "/time.txt");
       strTime.toCharArray(JSONmessageBufferTime,strTime.length()+1);
       Serial.println(JSONmessageBufferTime);
       client.publish(timeIntervalTopic1,JSONmessageBufferTime);
       free(p);
    }else{
       JsonObject& parsedTime = JSONBuffertime.parseObject(String(timeInterval));
       timeJSONencoder["timeInterval"] = parsedTime["timeInterval"];
       timeJSONencoder.printTo(timeJSONmessageBuffer, sizeof(timeJSONmessageBuffer));
       deleteFile(SPIFFS, "/time.txt");
       Serial.println(timeJSONmessageBuffer);
       writeFile(SPIFFS, "/time.txt", timeJSONmessageBuffer);
       char JSONmessageBufferTime[500];
       String strTime = readFile(SPIFFS, "/time.txt");
       strTime.toCharArray(JSONmessageBufferTime,strTime.length()+1);
       Serial.println(JSONmessageBufferTime);
       client.publish(timeIntervalTopic1,JSONmessageBufferTime);
       free(p);
    }
  }
  if (strcmp(topic,deviceInfoTopicUpdate1)==0) {
    if(!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED)){
      Serial.println("SPIFFS Mount Failed");
      return;
    }
    char JSONmessageBuffer[300];
    StaticJsonBuffer<300> JSONbuffer;
    JsonObject& JSONencoder = JSONbuffer.createObject();
    byte* p = (byte*)malloc(length);
    memcpy(p,payload,length);
    device_name = (char*)p;
    StaticJsonBuffer<300> JSONBufferDevice;
   if(SPIFFS.exists("/deviceinfo.txt")){
       JsonObject& parsedDevice = JSONBufferDevice.parseObject(String(device_name));
       JSONencoder["deviceName"] = parsedDevice["device_name"];
       JSONencoder["deviceId"] = macaddress1;
       JSONencoder["macAddress"] = mac9;
       JSONencoder["commissionDate"] = dayStampStart;
       JSONencoder.printTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
       deleteFile(SPIFFS, "/deviceinfo.txt");
       Serial.println(JSONmessageBuffer);
       writeFile(SPIFFS, "/deviceinfo.txt", JSONmessageBuffer);
       char JSONmessageBuffer[500];
       String str = readFile(SPIFFS, "/deviceinfo.txt");
       str.toCharArray(JSONmessageBuffer,str.length()+1);
       Serial.println(JSONmessageBuffer);
       client.publish(deviceInfoTopic1,JSONmessageBuffer);
       free(p);
    }else{
       JsonObject& parsedDevice = JSONBufferDevice.parseObject(String(device_name));
       JSONencoder["deviceName"] = parsedDevice["device_name"];
       JSONencoder["deviceId"] = macaddress1;
       JSONencoder["macAddress"] = mac9;
       JSONencoder["commissionDate"] = dayStampStart;
       JSONencoder.printTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
       writeFile(SPIFFS, "/deviceinfo.txt", JSONmessageBuffer);
       char JSONmessageBuffer[500];
       String str = readFile(SPIFFS, "/deviceinfo.txt");
       str.toCharArray(JSONmessageBuffer,str.length()+1);
       client.publish(deviceInfoTopic1,JSONmessageBuffer);
       free(p);
    }
  }  
  if (strcmp(topic,channleTopicInitiate1)==0){
    Serial.println("Got payload from channelPayload");
    if(SPIFFS.exists("/channels.txt")){
       Serial.println("File already Exists");
       char JSONmessageBuffer1[3000];
       String str = readFile(SPIFFS, "/channels.txt");
       Serial.println(str);
       str.toCharArray(JSONmessageBuffer1,str.length()+2);
       client.publish(channleTopic1,JSONmessageBuffer1);
     }else{
        client.publish(channleTopic1,"No Channel Data Found");
    }
  }

  if (strcmp(topic,timeIntervalTopicInitiate1)==0) {
    if(SPIFFS.exists("/time.txt")){
      Serial.println("File already exists");
      char timejsonbuffer[200];
      String timestr = readFile(SPIFFS, "/time.txt");
      timestr.toCharArray(timejsonbuffer, timestr.length()+2);
      client.publish(timeIntervalTopic1,timejsonbuffer);
    }else{
      client.publish(timeIntervalTopic1,"No Time Interval Set");
    }
  }
  
  if (strcmp(topic,deviceInfoTopicInitiate1)==0) {
    if(SPIFFS.exists("/deviceinfo.txt")){
       Serial.println("File already Exists");
       char JSONmessageBuffer2[1000];
       String str = readFile(SPIFFS, "/deviceinfo.txt");
       str.toCharArray(JSONmessageBuffer2,str.length()+1);
       Serial.println(JSONmessageBuffer2);
       client.publish(deviceInfoTopic1,JSONmessageBuffer2);
     }else{
       client.publish(deviceInfoTopic1,"No DeviceInfo Found");
    }
  }  
}

void preTransmission()
{
  MySerial1.flush();
  digitalWrite(SSerialTxControl, HIGH);
  Serial.println("Transmit On");
}
void postTransmission()
{
  delayMicroseconds(120);
  digitalWrite(SSerialTxControl, LOW);
  Serial.println("Transmit Off");
}

float bit_32(int reg0, int reg1){
  unsigned char words[4];
  words[3] = reg0 >> 8; //43
  words[2] = reg0 & 0xFF; //62
  words[1] = reg1 >> 8; //d7
  words[0] = reg1 & 0xFF; //59
  float* myfloat = (float *) &words;
  //Serial.println(*myfloat);
  return *myfloat;
}

double bit_64(int reg0, int reg1, int reg2, int reg3){
  unsigned char words[8];
  words[7] = reg0 >> 8;
  words[6] = reg0 & 0xFF;
  words[5] = reg1 >> 8;
  words[4] = reg1 & 0xFF;
  words[3] = reg2 >> 8;
  words[2] = reg2 & 0xFF;
  words[1] = reg3 >> 8;
  words[0] = reg3 & 0xFF;

  double* mydouble = (double *) &words;
  //Serial.println(*mydouble);
  return *mydouble;
}

void mqttconnect() {
  /* Loop until reconnected */
  if(!client.connected()) {
    Serial.print("MQTT connecting ...");
    /* client ID */
//    String clientId = "ESP32Client";
//    byte willQoS = 1;
//    const char* willTopic = "meterReady";
//    const char* willMessage = "Meter Ready";
//    boolean willRetain = true;
    /* connect now */
//    if (client.connect(macaddress1, mqttUser, mqttPassword,willTopic,willQoS,willRetain,willMessage)) {
  if (client.connect(macaddress1, mqttUser, mqttPassword)) {
      Serial.println(macaddress1);
      Serial.println(channleTopic1);
      Serial.println(deviceInfoTopic1);
      Serial.println(timeIntervalTopic);
      Serial.println(channleTopicInitiate1);
      Serial.println(timeIntervalTopicInitiate1);
      Serial.println(deviceInfoTopicInitiate1);
      Serial.println(channleTopicUpdate1);
      Serial.println(timeIntervalTopicUpdate1);
      Serial.println(deviceInfoTopicUpdate1);
      /* subscribe topic with default QoS 0*/
      client.subscribe(TEMP_TOPIC);
      client.subscribe(channleTopic1);
      client.subscribe(timeIntervalTopic1);
      client.subscribe(deviceInfoTopic1);
      client.subscribe(channleTopicInitiate1);
      client.subscribe(timeIntervalTopicInitiate1);
      client.subscribe(deviceInfoTopicInitiate1,1);
      client.subscribe(channleTopicUpdate1);
      client.subscribe(timeIntervalTopicUpdate1);
      client.subscribe(deviceInfoTopicUpdate1);
      StaticJsonBuffer<300> JSONbuffer;
      char response[100];
      JsonObject& JSONencoder = JSONbuffer.createObject();
      JSONencoder["success"] = true;
      JSONencoder["message"] = "Ready to Communicate";
      JSONencoder.printTo(response, 100);
      client.publish(meterStatus1,response);
//      client.publish(meterStatus1,"Meter Redy");
      Serial.println("connected");
      //LOG
    } else {
      Serial.print("failed, status code =");
      Serial.print(client.state());
      //Serial.print(client);
      Serial.println("try again in 5 seconds");
      //LOG
      /* Wait 5 seconds before retrying */
      //delay(5000);
    }
  }
}

void createDir(fs::FS &fs, const char * path){
    Serial.printf("Creating Dir: %s\n", path);
    if(fs.mkdir(path)){
        Serial.println("Dir created");
    } else {
        Serial.println("mkdir failed");
    }
}

void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
    Serial.printf("Listing directory: %s\r\n", dirname);

    File root = fs.open(dirname);
    if(!root){
        Serial.println("- failed to open directory");
        return;
    }
    if(!root.isDirectory()){
        Serial.println(" - not a directory");
        return;
    }

    File file = root.openNextFile();
    while(file){
        if(file.isDirectory()){
            Serial.print("  DIR : ");
            Serial.println(file.name());
            if(levels){
                listDir(fs, file.name(), levels -1);
            }
        } else {
            Serial.print("  FILE: ");
            Serial.print(file.name());
            Serial.print("  SIZE: ");
            Serial.println(file.size());
        }
        file = root.openNextFile();
    }
}

void writeFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Writing file: %s\n", path);

    File file = fs.open(path, FILE_WRITE);
    if(!file){
        Serial.println("Failed to open file for writing");
        return;
    }
    if(file.print(message)){
        Serial.println("File written");
    } else {
        Serial.println("Write failed");
    }
}

void deleteFile(fs::FS &fs, const char * path){
    Serial.printf("Deleting file: %s\n", path);
    if(fs.remove(path)){
        Serial.println("File deleted");
    } else {
        Serial.println("Delete failed");
    }
}


void appendFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Appending to file: %s\n", path);

    File file = fs.open(path, FILE_APPEND);
    if(!file){
        Serial.println("Failed to open file for appending");
        return;
    }
    if(file.print(message)){
        Serial.println("Message appended");
    } else {
        Serial.println("Append failed");
    }
    file.close();
}


String readFile(fs::FS &fs, const char * path){
    Serial.printf("Reading file: %s\n", path);

    File file = fs.open(path);
    if(!file){
        Serial.println("Failed to open file for reading");
        return "  ";
    }

    Serial.print("Read from file: ");
    while(file.available()){
        //Serial.write(file.read());
        return file.readString();
    }
    file.close();
}


void eraseFile(fs::FS &fs, const char * path){
    Serial.printf("Reading file: %s\n", path);

    File file = fs.open(path);
    if(!file){
        Serial.println("Failed to open file for reading");
        return;
    }

    Serial.print("Erase file: ");
    while(file.available()){
        //Serial.write(file.read());
        String data1 = file.readString();
        data1.replace(data1,":");
        file.print(data1);
        //return file.readString();
    }
    file.close();
}


void renameFile(fs::FS &fs, const char * path1, const char * path2){
    Serial.printf("Renaming file %s to %s\n", path1, path2);
    if (fs.rename(path1, path2)) {
        Serial.println("File renamed");
    } else {
        Serial.println("Rename failed");
    }
}

void delLine( File f, uint32_t line ){ // line is 1..n, no Zero Line!
uint32_t S= (line-1)*LINELEN;
Serial.println("Deleting line");
f.seek(0);  //position the 'file pointer' to where I want to start over-writing.
char ch[LINELEN+1]; // got to remember c++ strings need a \0 to mark the end.
// build the 'delete line'
for(uint8_t i=0;i<LINELEN-2;i++){ ch[i]=' ';}
ch[LINELEN-2]='\r'; //  (cr)
ch[LINELEN-3]='\n'; // newline (lf)
ch[LINELEN]='\0'; // end of string marker;
// over write (delete) the existing data
f.print(ch); // all marked as deleted! yea!
}

char *stringToChar(String data){
  char *charArray = (char *) malloc(500);
  data.toCharArray(charArray, 500);
  return charArray;
}

char *createJson(String data, const char *message){
  char *charArray = (char *) malloc(500);
  StaticJsonBuffer<300> JSONbuffer;
  JsonObject& JSONencoder = JSONbuffer.createObject();
  JSONencoder["timeStamp"] = data;
  JSONencoder["message"] = message;
  JSONencoder.printTo(charArray, 500);
  Serial.println(charArray);
  return charArray;
}

void coreTask( void * pvParameters ){
    while(true){
     if(retriveFlag == "True"){
     File printFile;
     if(SD_MMC.exists("/retrivalData.txt")){
      printFile = SD_MMC.open("/retrivalData.txt");
      if(printFile.available()) {
        String line = printFile.readStringUntil('\n');
        char JSONmessageBufferRetriveData[5000];
        line.toCharArray(JSONmessageBufferRetriveData,line.length()+1);
        client.publish(retriveData1,JSONmessageBufferRetriveData);
        Serial.println(line); //Printing for debugging purpose
        LINELEN = line.length();
        delLine(printFile,1);
       }else{
          retriveFlag = "False";
        }
      }
    }
        if(!timeClient.update()) {
          Serial.println("Updatinf NTP Client");
        timeClient.forceUpdate();
        //LOG
        if(SD_MMC.exists(filePathLog1)){
          File fileLog = SD_MMC.open(filePathLog1, FILE_APPEND);
          fileLog.print(formattedDate);
          fileLog.print(",");
          fileLog.print("NTP client UpdateFailed");
          fileLog.print("\n");
          char * p = createJson(formattedDate,"NTP client UpdateFailed");
          Serial.println (p);
          client.publish(retriveErrorLog1,p);
          free (p);
        }else{
          File fileLog = SD_MMC.open(filePath1, FILE_WRITE);
          fileLog.print(formattedDate);
          fileLog.print(",");
          fileLog.print("NTP client UpdateFailed");
          fileLog.print("\n");
          char * p = createJson(formattedDate,"NTP client UpdateFailed");
          Serial.println (p);
          client.publish(retriveErrorLog1,p);
          free (p);
        }
        }
        unixtim = timeClient.getEpochTime();
        formattedTime = timeClient.getFormattedTime();
        formattedDate = timeClient.getFormattedDate();
        splitT = formattedDate.indexOf("T");
        dayStamp = formattedDate.substring(0, splitT);
        folderPath = "/";
        hrs = String(timeClient.getHours());
        Serial.println(hrs);
        filePath = "";
        String slash = "/";
        String extension = ".csv";
        folderPath.concat(dayStamp);
        filePathLog = folderPath;
        filePathLog.concat("/");
        filePathLog.concat(folderPath);
        filePathLog.concat("-log.csv");
        filePathLog.toCharArray(filePathLog1,filePathLog.length()+1);
        folderPath.toCharArray(folderPath1,folderPath.length()+1);
        filePath.concat(folderPath);
        filePath.concat(slash);
        filePath.concat(hrs);
        filePath.concat(extension);
        filePath.toCharArray(filePath1,filePath.length()+1);
        if(SD_MMC.exists(folderPath)){
          if(!SD_MMC.exists(filePath1)){
            String param_listdata_logging[] = {"timestamp","channel_no","valid", "sensor", "volt","freq","phase","amp","watt","watthr","var","varhr","degc"};
          //if(!SD_MMC.exists("/data.csv")){
            int l;
            File file1 = SD_MMC.open(filePath1, FILE_WRITE);
            if(!file1){
              Serial.println("Failed to open file for writing");
              return;
            }
            for(l=0;l<13;l++){
                    if(file1.print(param_listdata_logging[l])){
                      if(l != 12){
                       file1.print(","); 
                      }
              Serial.println("File written");
            } else {
              Serial.println("Write failed");
              }
            }
            file1.print("\n");
            file1.close();
          //}
          }
        }else{
          createDir(SD_MMC,folderPath1);
          String param_listdata_logging[] = {"timestamp","channel_no","valid", "sensor", "volt","freq","phase","amp","watt","watthr","var","varhr","degc"};
            int l;
            Serial.println(filePath1);
            File file1 = SD_MMC.open(filePath1, FILE_WRITE);
            if(!file1){
              Serial.println("Failed to open file for writing");
              return;
            }
            for(l=0;l<13;l++){
                    if(file1.print(param_listdata_logging[l])){
                      if(l != 12){
                       file1.print(","); 
                      }
              Serial.println("File written");
            } else {
              Serial.println("Write failed");
              }
            }
            file1.print("\n");
            file1.close();
        }
        /* Modbus resisters read */
        if(SPIFFS.exists("/channels.txt")){
             Serial.println("Channels File already Exists");
             StaticJsonBuffer<2500> JSONBufferChannels;
             JsonObject& parsedChannels = JSONBufferChannels.parseObject(readFile(SPIFFS, "/channels.txt"));
             int l;
             len_channel = parsedChannels["count"];
             Serial.println(len_channel);
            for(l = 0; l<len_channel; l++){
                StaticJsonBuffer<500> JSONBufferChannels1;
                String channelDetails = parsedChannels["channels"][l];
                JsonObject& parsedChannels1 = JSONBufferChannels1.parseObject(channelDetails);
                //strcpy(channels[l],parsedChannels1["no"]);
                String s = parsedChannels1["no"];
                //channels[l] = (String)parsedChannels1["no"];
                //String s = char_channel;
                channels[l] = s;
                Serial.println(channels[l]);
                //channelName[l] = parsedChannels1["name"];
              }
          }
//          Serial.println(channels);
//          #define NUMITEMS(arg) ((unsigned int) (sizeof (arg) / sizeof (arg [0])))
          int k;
          Serial.println("number of channels is:");
          StaticJsonBuffer<350> JSONBuffer7;
//          Serial.println(readFile(SPIFFS, "/registers.json"));
          char registersData[300];
          String registers = readFile(SPIFFS, "/registers.json");
          registers.toCharArray(registersData,300);
          JsonObject& parsed7 = JSONBuffer7.parseObject(registersData);
          Serial.println(registersData);
          uint8_t result;
          int data[24];
          int j;
          char *channelChar;
          
          //deleteFile(SD_MMC,"/data.csv");
          
          
          File file = SD_MMC.open(filePath, FILE_APPEND);
          if(!file){
              Serial.println("Failed to open file for appending");
              return;
          }
          String param_list[] = {"valid", "sensor", "volt","freq","phase","amp","watt","watthr","var","varhr","degc"};
          DynamicJsonBuffer jsonBuffer;
          JsonObject& root = jsonBuffer.createObject();
          JsonArray& networks = root.createNestedArray("channels");
          root["timeStamp"] = unixtim;
      
          for(k = 0; k< len_channel; k++){
            file.print(unixtim);
            file.print(",");
            channels[k].toCharArray(channelChar,channels[k].length());
            Serial.println("Channel number is:");
            Serial.print(channelChar);
            file.print(channelChar);
            file.print(",");
            JsonObject& network = networks.createNestedObject();
            network["channel"] = channels[k];
            int reg = int(parsed7[channels[k]]);
            Serial.println(reg);
            result = node.readHoldingRegisters(reg,24);
            Serial.println(result);
            if(SD_MMC.exists(filePathLog)){
              File fileLog = SD_MMC.open(filePathLog1, FILE_APPEND);
              fileLog.print(formattedDate);
              fileLog.print(",");
              if(result == 1){
                fileLog.print("Modbus protocol illegal function exception.");
                char * p = createJson(formattedDate,"Modbus protocol illegal function exception.");
                Serial.println (p);
                client.publish(retriveErrorLog1,p);
                free (p);
              }else if(result == 2){
                fileLog.print("Modbus protocol illegal data address exception.");
                char * p = createJson(formattedDate,"Modbus protocol illegal data address exception.");
                Serial.println (p);
                client.publish(retriveErrorLog1,p);
                free (p);
              }else if(result == 3){
                fileLog.print("Modbus protocol illegal data value exception.");
                char * p = createJson(formattedDate,"Modbus protocol illegal data value exception.");
                Serial.println (p);
                client.publish(retriveErrorLog1,p);
                free (p);
              }else if(result == 4){
                fileLog.print("Modbus protocol slave device failure exception.");
                char * p = createJson(formattedDate,"Modbus protocol slave device failure exception.");
                Serial.println (p);
                client.publish(retriveErrorLog1,p);
                free (p);
              }else if(result == 224){
                fileLog.print("ModbusMaster invalid response slave ID exception.");
                char * p = createJson(formattedDate,"ModbusMaster invalid response slave ID exception.");
                Serial.println (p);
                client.publish(retriveErrorLog1,p);
                free (p);
              }else if(result == 225){
                fileLog.print("ModbusMaster invalid response function exception.");
                char * p = createJson(formattedDate,"ModbusMaster invalid response function exception.");
                Serial.println (p);
                client.publish(retriveErrorLog1,p);
                free (p);
              }else if(result == 226){
                fileLog.print("ModbusMaster response timed out exception.");
                char * p = createJson(formattedDate,"ModbusMaster response timed out exception.");
                Serial.println (p);
                client.publish(retriveErrorLog1,p);
                free (p);
              }else if(result == 227){
                fileLog.print("ModbusMaster invalid response CRC exception.");
                char * p = createJson(formattedDate,"ModbusMaster invalid response CRC exception.");
                Serial.println (p);
                client.publish(retriveErrorLog1,p);
                free (p);
              }
              fileLog.print("\n");
            }else{
              File fileLog = SD_MMC.open(filePathLog1, FILE_WRITE);
              fileLog.print(formattedDate);
              fileLog.print(",");
              if(result == 1){
                fileLog.print("Modbus protocol illegal function exception.");
                char * p = createJson(formattedDate,"Modbus protocol illegal function exception.");
                Serial.println (p);
                client.publish(retriveErrorLog1,p);
                free (p);
              }else if(result == 2){
                fileLog.print("Modbus protocol illegal data address exception.");
                char * p = createJson(formattedDate,"Modbus protocol illegal data address exception.");
                Serial.println (p);
                client.publish(retriveErrorLog1,p);
                free (p);
              }else if(result == 3){
                fileLog.print("Modbus protocol illegal data value exception.");
                char * p = createJson(formattedDate,"Modbus protocol illegal data value exception.");
                Serial.println (p);
                client.publish(retriveErrorLog1,p);
                free (p);
              }else if(result == 4){
                fileLog.print("Modbus protocol slave device failure exception.");
                char * p = createJson(formattedDate,"Modbus protocol slave device failure exception.");
                Serial.println (p);
                client.publish(retriveErrorLog1,p);
                free (p);
              }else if(result == 224){
                fileLog.print("ModbusMaster invalid response slave ID exception.");
                char * p = createJson(formattedDate,"ModbusMaster invalid response slave ID exception.");
                Serial.println (p);
                client.publish(retriveErrorLog1,p);
                free (p);
              }else if(result == 225){
                fileLog.print("ModbusMaster invalid response function exception.");
                char * p = createJson(formattedDate,"ModbusMaster invalid response function exception.");
                Serial.println (p);
                client.publish(retriveErrorLog1,p);
                free (p);
              }else if(result == 226){
                fileLog.print("ModbusMaster response timed out exception.");
                char * p = createJson(formattedDate,"ModbusMaster response timed out exception.");
                Serial.println (p);
                client.publish(retriveErrorLog1,p);
                free (p);
              }else if(result == 227){
                fileLog.print("ModbusMaster invalid response CRC exception.");
                char * p = createJson(formattedDate,"ModbusMaster invalid response CRC exception.");
                Serial.println (p);
                client.publish(retriveErrorLog1,p);
                free (p);
              }
              fileLog.print("\n");
            }
            if (result == node.ku8MBSuccess)
            {
               for (j = 0; j < 24; j++)
                 {
                   data[j] = node.getResponseBuffer(j);
                 }
            }
      
            double final_data[11];
            int i1;
            int j1;
            int prev_flag = 1, flag = 0;
            int sequence[11] = {1,1,2,2,2,2,2,4,2,4,2};
            for(i1 = 0; i1<11; i1++){
              int reg[4];
              for(j1 = 0; j1<sequence[i1]; j1++){
                reg[j1] = data[flag];
                flag = flag + 1;
              }
              if(sequence[i1] == 2){
                 //Serial.println(bit_32(reg[0],reg[1]));
                 final_data[i1] = bit_32(reg[0],reg[1]);
                 file.print(final_data[i1]);
                 if(i1 == 10){
                  file.print("\n");
                 }else{
                  file.print(",");
                 }
                 
              }else if(sequence[i1] == 4){
                //Serial.println(bit_64(reg[0],reg[1],reg[2],reg[3]));
                final_data[i1] = bit_64(reg[0],reg[1],reg[2],reg[3]);
                 file.print(final_data[i1]);
                 file.print(",");
              }else{
                //Serial.println(reg[0]);
                final_data[i1] = reg[0];
                 file.print(final_data[i1]);
                 file.print(",");
              }
              network[param_list[i1]] = final_data[i1];
            }
         //file.print("\n");
         String taskMessage = "Publishing Task running on core ";
         taskMessage = taskMessage + xPortGetCoreID();
         Serial.println(taskMessage);
         delay(2000);
    }
    file.close();
    char JSONmessageBufferChannelData[5000];
    root.printTo(JSONmessageBufferChannelData, sizeof(JSONmessageBufferChannelData));
    File retrival;
    if(!client.publish(channelData1,JSONmessageBufferChannelData)){
      if(!SD_MMC.exists("/retrivalData.txt")){
        retrival = SD_MMC.open("/retrivalData.txt", FILE_APPEND);
        retrival.print("\n");
        retrival.print(JSONmessageBufferChannelData);
      }else{
        retrival = SD_MMC.open("/retrivalData.txt", FILE_WRITE);
        retrival.print(JSONmessageBufferChannelData);
      }
    }
    delay(55000);  
      }
   }
void setup() {
  Serial.begin(115200);
  /* Set ESP32 to WiFi Station mode */
//  if (WiFi.SSID()) {
//      Serial.println(WiFi.psk());
//      WiFi.getAutoConnect();
//      Serial.println("Using last saved values, should be faster");
//      
//  }
  WiFi.mode(WIFI_AP_STA);
  /* start SmartConfig */
  WiFi.beginSmartConfig();
  /* Wait for SmartConfig packet from mobile */
  Serial.println("Waiting for SmartConfig.");
  while (!WiFi.smartConfigDone()) {
    delay(500);
    Serial.print(".");
  }
//  String ssid1 = WiFi.SSID();
//  String password1 = WiFi.psk();
//  ssid1.toCharArray(ssid,ssid1.length());
//  password1.toCharArray(password,password1.length());
  Serial.println("");
  Serial.println("SmartConfig done.");
  /* Wait for WiFi to connect to AP */
  Serial.println("Waiting for WiFi");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi Connected.");
  //LOG
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());
  WiFi.macAddress(mac);
  Serial.println("mac address is:");
  String mac3 = WiFi.macAddress();
  String mac4 = mac3;
  mac3.replace(":","");
  mac3.toLowerCase();
  String macaddress = mac3;
  timeClient.begin();
  while(!timeClient.update()) {
        timeClient.forceUpdate();
        Serial.println("updating NTP client");
        //LOG
        if(SD_MMC.exists(filePathLog)){
          File fileLog = SD_MMC.open(filePathLog1, FILE_APPEND);
          fileLog.print(formattedDate);
          fileLog.print(",");
          fileLog.print("NTP client UpdateFailed");
          fileLog.print("\n");
          if(SD_MMC.exists("/retrievalErrorLog.txt")){
            File fileLog = SD_MMC.open("/retrievalErrorLog.txt", FILE_APPEND);
            fileLog.print(formattedDate);
            fileLog.print(",");
            fileLog.print("NTP client UpdateFailed");
            fileLog.print("\n");
        }else{
            File fileLog = SD_MMC.open("/retrievalErrorLog.txt", FILE_WRITE);
            fileLog.print(formattedDate);
            fileLog.print(",");
            fileLog.print("NTP client UpdateFailed");
            fileLog.print("\n");
        }
        }else{
          File fileLog = SD_MMC.open(filePathLog1, FILE_WRITE);
          fileLog.print(formattedDate);
          fileLog.print(",");
          fileLog.print("NTP client UpdateFailed");
          fileLog.print("\n");
          if(SD_MMC.exists("/retrievalErrorLog.txt")){
            File fileLog = SD_MMC.open("/retrievalErrorLog.txt", FILE_APPEND);
            fileLog.print(formattedDate);
            fileLog.print(",");
            fileLog.print("NTP client UpdateFailed");
            fileLog.print("\n");
        }else{
            File fileLog = SD_MMC.open("/retrievalErrorLog.txt", FILE_WRITE);
            fileLog.print(formattedDate);
            fileLog.print(",");
            fileLog.print("NTP client UpdateFailed");
            fileLog.print("\n");
        }
        }
  }
  unixtim = timeClient.getEpochTime();
  formattedTime = timeClient.getFormattedTime();
  formattedDate = timeClient.getFormattedDate();
  splitTStart = formattedDate.indexOf("T");
  dayStampStart = formattedDate.substring(0, splitTStart);
  Serial.println(dayStampStart);
  
  splitT = formattedDate.indexOf("T");
  dayStamp = formattedDate.substring(0, splitT);
  folderPath = "/";
  hrs = String(timeClient.getHours());
  folderPath.concat(dayStamp);
  if(!SD_MMC.begin()){
     Serial.println("Card Mount Failed");
     char * p = createJson(formattedDate,"Card Mount Failed");
     Serial.println (p);
     client.publish(retriveErrorLog1,p);
     free (p);
     //return;
     //LOG
     }
  if(!SD_MMC.exists(folderPath)){
    createDir(SD_MMC,folderPath1);
  }
  filePathLog = folderPath;
  filePathLog.concat(folderPath);
  filePathLog.concat("-log.csv");
  filePathLog.toCharArray(filePathLog1,filePathLog.length()+1);
  Serial.println(filePathLog1);
  channleTopic = String("channel")+String("/")+macaddress;
  timeIntervalTopic = String("timeIntervalInfo")+String("/")+macaddress;
  deviceInfoTopic = String("deviceInfo")+String("/")+macaddress;
  channleTopicInitiate = String("channelInitiate")+String("/")+macaddress;
  timeIntervalTopicInitiate = String("timeIntervalInitiate")+String("/")+macaddress;
  deviceInfoTopicInitiate = String("deviceInfoInitiate")+String("/")+macaddress;
  channleTopicUpdate = String("channelUpdate")+String("/")+macaddress;
  timeIntervalTopicUpdate = String("timeIntervalUpdate")+String("/")+macaddress;
  deviceInfoTopicUpdate = String("deviceInfoUpdate")+String("/")+macaddress;
  meterStatus = String("meterReadyToCommunicate/") + macaddress;
  retriveData = String("retrieveData/") + macaddress;
  retriveErrorLog = String("retrieveErrorLog/") + macaddress;
  channelData = String("channelData/") + macaddress;

  mac4.toCharArray(mac9,mac4.length()+1);
  macaddress.toCharArray(macaddress1,macaddress.length()+1);
  channleTopic.toCharArray(channleTopic1,channleTopic.length()+1);
  timeIntervalTopic.toCharArray(timeIntervalTopic1,timeIntervalTopic.length()+1);
  deviceInfoTopic.toCharArray(deviceInfoTopic1,deviceInfoTopic.length()+1);
  channleTopicInitiate.toCharArray(channleTopicInitiate1,channleTopicInitiate.length()+1);
  timeIntervalTopicInitiate.toCharArray(timeIntervalTopicInitiate1,timeIntervalTopicInitiate.length()+1);
  deviceInfoTopicInitiate.toCharArray(deviceInfoTopicInitiate1,deviceInfoTopicInitiate.length()+1);
  channleTopicUpdate.toCharArray(channleTopicUpdate1,channleTopicUpdate.length()+1);
  timeIntervalTopicUpdate.toCharArray(timeIntervalTopicUpdate1,timeIntervalTopicUpdate.length()+1);
  deviceInfoTopicUpdate.toCharArray(deviceInfoTopicUpdate1,deviceInfoTopicUpdate.length()+1);
  meterStatus.toCharArray(meterStatus1,meterStatus.length()+1);
  retriveData.toCharArray(retriveData1,retriveData.length()+1);
  retriveErrorLog.toCharArray(retriveErrorLog1,retriveErrorLog.length()+1);
  channelData.toCharArray(channelData1,channelData.length()+1);
  
  client.setServer(mqtt_server, 10302);
//client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  mqttconnect();
  while(!client.connected()) {
     Serial.println("Mqtt connecting from the setup");
     if(SD_MMC.exists(filePathLog1)){
          File fileLog = SD_MMC.open(filePathLog1, FILE_APPEND);
          fileLog.print(formattedDate);
          fileLog.print(",");
          fileLog.print("Mqtt Failed");
          fileLog.print("\n");
        if(SD_MMC.exists("/retrievalErrorLog.txt")){
          File fileLog = SD_MMC.open("/retrievalErrorLog.txt", FILE_APPEND);
          fileLog.print(formattedDate);
          fileLog.print(",");
          fileLog.print("Mqtt Failed");
          fileLog.print("\n");
        }else{
          File fileLog = SD_MMC.open("/retrievalErrorLog.txt", FILE_WRITE);
          fileLog.print(formattedDate);
          fileLog.print(",");
          fileLog.print("Mqtt Failed");
          fileLog.print("\n");
        }
        }else{
          File fileLog = SD_MMC.open(filePathLog1, FILE_WRITE);
          fileLog.print(formattedDate);
          fileLog.print(",");
          fileLog.print("Mqtt Failed");
          fileLog.print("\n");
          if(SD_MMC.exists("/retrievalErrorLog.txt")){
          File fileLog = SD_MMC.open("/retrievalErrorLog.txt", FILE_APPEND);
          fileLog.print(formattedDate);
          fileLog.print(",");
          fileLog.print("Mqtt Failed");
          fileLog.print("\n");
        }else{
          File fileLog = SD_MMC.open("/retrievalErrorLog.txt", FILE_WRITE);
          fileLog.print(formattedDate);
          fileLog.print(",");
          fileLog.print("Mqtt Failed");
          fileLog.print("\n");
        }
        }
     //client.loop();
  }

  if(WiFi.status() == WL_CONNECTED){
    Serial.println("Hi this is being logged");
     if(SD_MMC.exists(filePathLog1)){
          Serial.println("hiiiii");
          File fileLog = SD_MMC.open(filePathLog1, FILE_APPEND);
          fileLog.print(formattedDate);
          fileLog.print(",");
          fileLog.print("WiFi Connected");
          fileLog.print("\n");
          char * p = createJson(formattedDate,"WiFi Connected");
          Serial.println (p);
          if(!client.publish(retriveErrorLog1,p)){
            if(SD_MMC.exists("/retrievalErrorLog.txt")){
              File fileLog = SD_MMC.open("/retrievalErrorLog.txt", FILE_APPEND);
              fileLog.print(formattedDate);
              fileLog.print(",");
              fileLog.print("Mqtt Failed");
              fileLog.print("\n");
            }else{
              File fileLog = SD_MMC.open("/retrievalErrorLog.txt", FILE_WRITE);
              fileLog.print(formattedDate);
              fileLog.print(",");
              fileLog.print("Mqtt Failed");
              fileLog.print("\n");
            }
          }
          free (p);
        }else{
          Serial.println("hiiiii");
          File fileLog = SD_MMC.open(filePathLog1, FILE_WRITE);
          fileLog.print(formattedDate);
          fileLog.print(",");
          fileLog.print("WiFi Connected");
          fileLog.print("\n");
          client.publish("retrieve",createJson(formattedDate,"WiFi Connected"));
          char * p = createJson(formattedDate,"WiFi Connected");
          Serial.println (p);
          if(!client.publish(retriveErrorLog1,p)){
            if(SD_MMC.exists("/retrievalErrorLog.txt")){
              File fileLog = SD_MMC.open("/retrievalErrorLog.txt", FILE_APPEND);
              fileLog.print(formattedDate);
              fileLog.print(",");
              fileLog.print("Mqtt Failed");
              fileLog.print("\n");
            }else{
              File fileLog = SD_MMC.open("/retrievalErrorLog.txt", FILE_WRITE);
              fileLog.print(formattedDate);
              fileLog.print(",");
              fileLog.print("Mqtt Failed");
              fileLog.print("\n");
            }
          }
          free (p);
        }
  }
  
  MySerial1.begin(115200, SERIAL_8N1,16,17);
  node.begin(1,MySerial1);
  pinMode(SSerialTxControl, OUTPUT); 
  digitalWrite(SSerialTxControl, HIGH);
  // Callbacks allow us to configure the RS485 transceiver correctly
  node.preTransmission(preTransmission);
  node.postTransmission(postTransmission);
  //Serial.println("Writing register config file");
  if(!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED)){
        Serial.println("SPIFFS Mount Failed");
        return;
        }
  deleteFile(SPIFFS, "/registers.json");
  if(!SPIFFS.exists("/registers.json")){
      StaticJsonBuffer<300> JSONbuffer8;
      JsonObject& JSONencoder8 = JSONbuffer8.createObject();
      JSONencoder8["CH_01"] = 520;
      JSONencoder8["CH_02"] = 544;
      JSONencoder8["CH_03"] = 568;
      JSONencoder8["CH_04"] = 592;
      JSONencoder8["CH_05"] = 616;
      JSONencoder8["CH_06"] = 640;
      JSONencoder8["CH_07"] = 664;
      JSONencoder8["CH_08"] = 688;
      JSONencoder8["CH_09"] = 712;
      JSONencoder8["CH_10"] = 736;
      JSONencoder8["CH_11"] = 760;
      JSONencoder8["CH_12"] = 784;
      JSONencoder8["CH_13"] = 808;
      JSONencoder8["CH_14"] = 832;
      JSONencoder8["CH_15"] = 856;
      JSONencoder8["CH_16"] = 880;
      JSONencoder8["CH_17"] = 904;
      JSONencoder8["CH_18"] = 928;
      JSONencoder8["CH_19"] = 954;
      char JSONmessageBuffer8[300];
      JSONencoder8.printTo(JSONmessageBuffer8, sizeof(JSONmessageBuffer8));
      writeFile(SPIFFS, "/registers.json", JSONmessageBuffer8);
    }
   String taskMessage = "Task running on core ";
   taskMessage = taskMessage + xPortGetCoreID();
   Serial.println(taskMessage);
   xTaskCreatePinnedToCore(
                  coreTask,
                  "coreTask",
                  10000,
                  NULL,
                  0,
                  NULL,
                  0);             
}
void loop() {
  client.loop();
  if(client.connected() != 1) {
          mqttconnect();
          client.loop();
          Serial.println("Mqtt disconneted, connected from the loop");
          Serial.println(client.connected());
          //LOG
        }  
  //Serial.println(client.state());
}
