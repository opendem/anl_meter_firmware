# ANL Meter Firmware for ESP32 equipped meters

ESP32 interfaces with the ANL meter to enable communication and configuration over WiFi.

Once flashed onto ESP32 chip, the meter can be configured for connectivity (e.g. SSID) and device settings using ANL meter commissioning app.

ANL Meter Commissioning App is available for widely supported smart phones platforms - Android and iOS.

## Steps to flash the code

* Install Arduino IDE - instructions and installers can be found [Arduino website](https://www.arduino.cc/en/Main/Software)

* Connect ESP32 using USB cable.

* Open project using Aduino IDE using INO file included in this directory.

* Make sure the board under `Main Menu > Tools > Board:` is set to `Olimex ESP32-GATEWAY`.

* Make sure the Upload Speed under `Main Menu > Tools > Upload Speed:` is set to `115200`.

* Use Arduino's IDE compile function to Compile the project.

* Use Arduino's IDE flash function to flash  the ESP32 chip.

