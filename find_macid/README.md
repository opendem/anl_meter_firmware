# Find MAC Address of ESP32
t
Please follow the instructions below to run a small program on ESP32 to obtain MAC id. MAC ID is being used at the backend for now for multiple purposes. This should be replaced in the final release by a UUID.

## Steps to flash the code

* Install Arduino IDE - instructions and installers can be found [Arduino website](https://www.arduino.cc/en/Main/Software)

* Connect ESP32 using USB cable.

* Open project using Aduino IDE using INO file included in this directory.

* Make sure the board under `Main Menu > Tools > Board:` is set to `Olimex ESP32-GATEWAY`.

* Make sure the Upload Speed under `Main Menu > Tools > Upload Speed:` is set to `115200`.

* Use Arduino's IDE compile function to Compile the project.

* Use Arduino's IDE flash function to flash  the ESP32 chip.

* Check `Serial Monitor` in the Arduino IDE for MAC address.

