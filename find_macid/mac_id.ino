#include <SPI.h>
#include <WiFi.h>

char ssid[] = "yourNetwork";     // the name of your network
char password[] = "yourPassword"
int status = WL_IDLE_STATUS;     // the Wifi radio's status

void setup()
{
 Serial.begin(115200);
 WiFi.begin(ssid, password);
 while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
  // Once you are connected, print your MAC address:
  String macid = WiFi.macAddress();
  Serial.print("MAC: ");
  Serial.print(macid);
}

void loop () {}
